# filter_list

* Filters any list of same items according to Django querying functions
* Supports multi level objects such as book__author__name
* Does not alter the sorting order

## Usage

    filter_list(list, kwargs)

Kwargs should be provided according to Django ORM convention:

    filter_list(teams, name='Liverpool FC')
    filter_list(teams, name__contains='Bar')

Also supports multi level objects:

    filter_list(teams, city__country__name='France')

** NOTE: Does NOT support foreign key comparisons as objects, instead compare based on PK fields **

For example, `filter_list(teams, owner__bank_account__pk=13)` works as intended but `filter_list(teams, owner__bank_account=account)` fails.


Patches and pull requests are welcomed.

Licensed under MIT License.
Copyright (c) 2014 Kubilay Eksioglu