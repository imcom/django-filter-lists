import operator
import mixins

class FilterOp(mixins.Kwargable):
    opname = None
    negate = False
    value = None
    attr = None


resolver = {
    'exact': operator.eq,
    'iexact': lambda a, b: a.lower() == b.lower(),
    'gt': operator.gt, 'gte': operator.ge,
    'lt': operator.lt, 'lte': operator.le,
    'contains': operator.contains,  # for strings
    'icontains': lambda a, b: b.lower() in a.lower(),
    'in': lambda a, b: a in b,  # for lists
    'startswith': lambda a, b: a.startswith(b), 'istartswith': lambda a, b: a.lower().startswith(b.lower()),
    'endswith': lambda a, b: a.endswith(b), 'iendswith': lambda a, b: a.lower().endswith(b.lower()),
    'range': lambda a, b: b[0] <= a <= b[1],
    'year': lambda a, b: a.year == b,
    'month': lambda a, b: a.month == b,
    'day': lambda a, b: a.day == b,
    'is_null': lambda a, b: (not a and b) or (a and not b),
}

def filter_list(list, **kwargs):
    """
    Filters any list of same items according to Django querying functions
    Supports multi level objects such as book__author__name
    Does not alter the sorting order

    NOTE: Does not support object comparisons, because of that you should write:
    book__pk__exact=searched_book.id or book_id__exact=searched_book.id

    @param: list        List of objects to be queried
    @param: **kwargs    Keyword arguments that will be used to query
    """
    filterops = []
    for keyword, value in kwargs.iteritems():
        negate = False
        opindex = keyword.rfind('__')
        opname = 'exact' if opindex == -1 else keyword[opindex+2:]
        attr = keyword[0:opindex] if opindex != -1 else keyword

        # resolve if operation exists, otherwise this is an exact op
        if opname.startswith('not_') and resolver.get(opname[4:], None) is not None:
            opname = opname[4:]
            negate = True

        filterops.append(FilterOp(opname=opname, negate=negate, value=value, attr=attr))

    def f(x):
        for fo in filterops:
            attr = reduce(getattr, fo.attr.split('__'), x)
            if fo.negate:
                if resolver[fo.opname](attr, fo.value):
                    return False
            else:
                d = resolver[fo.opname](attr, fo.value)
                if not d:
                    return False
        return True

    return filter(f, list)








