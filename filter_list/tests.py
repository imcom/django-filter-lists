import unittest
import mixins
from functions import filter_list

class MockObject(mixins.Kwargable):
    intval = 0
    strval = None

    def __unicode__(self):
        return "%s %s" % (self.intval, self.strval)

class TestListFilter(unittest.TestCase):

    list = []

    def setUp(self):
        for x in range(0, 20):
            self.list.append(MockObject(intval=x, strval='annnen'))

        for x in range(20, 30):
            self.list.append(MockObject(intval=x, strval='baska'))

    def test_filter_list(self):
        self.assertEqual(len(filter_list(self.list, intval__gte=15)), 15)
        self.assertEqual(len(filter_list(self.list, strval='annnen')), 20)
        self.assertEqual(len(filter_list(self.list, strval__contains='a')), 30)

if __name__ == '__main__':
    unittest.main()