class Kwargable(object):
    def __init__(self, *args, **kwargs):
        super(Kwargable, self).__init__()
        for k, a in kwargs.iteritems():
            setattr(self, k, a)