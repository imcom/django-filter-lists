from distutils.core import setup

setup(
    name='django-filter-list',
    version='0.1.1',
    author='Kubilay Eksioglu',
    author_email='kubilayeksioglu@gmail.com',
    packages=['filter_list'],
    scripts=[],
    url='http://bitbucket.org/kubilayeksioglu/filter-list/',
    license='LICENSE.txt',
    description='Filters any list of objects using Django convention',
    long_description=open('README.txt').read(),
    install_requires=[],
)
